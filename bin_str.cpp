/*
Copyright (C) 2014 shiro <shiro@worldofcorecraft.com>
This program comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE.
*/
#include "bin_str.h"
#include <openssl/bn.h>
#include <stdexcept>

namespace crypto
{

static int ascii_to_hex(char a)
{
    if (a >= '0' && a <= '9')
        a = a - '0';
    else
    {
        a = std::tolower(a);
        if (!(a >= 'a' && a <= 'f'))
            throw std::runtime_error("Invalid hex");
        a = a - 'a' + 10;
    }
    return a;
}

static char hex_to_ascii(int i)
{
    if (i > 16 || i < 0)
        throw std::runtime_error("Invalid hex");
    if (i >= 0 && i <= 9)
        return i + '0';
    return 'a' + (i - 10);
}

static unsigned char byte_from_hex(char a, char b)
{
    int _a, _b;
    _a = ascii_to_hex(a);
    _b = ascii_to_hex(b);
    return (_a << 4) | _b;
}

std::string bin_str::bits() const
{
    std::string s;

    for (auto v : data_)
        for (int i = 7; i >= 0; --i)
            s += (v & (1 << i)) != 0 ? '1' : '0';

    return s;
}

std::string bin_str::hex() const
{
    std::string s;
    s.resize(data_.size() * 2);

    for (int i = 0, j = 0; i < data_.size(); ++i, j += 2)
    {
        int hi = (data_[i] >> 4);
        int lo = data_[i] & ~0xF0;
        s[j] = hex_to_ascii(hi);
        s[j + 1] = hex_to_ascii(lo);
    }

    return s;
}

bin_str bin_str_hex(const std::string& hex)
{
    if (hex.size() % 2 != 0)
        throw std::runtime_error("bin_str_hex arg not evenly divisible by 2");

    std::vector<unsigned char> v;
    for (std::size_t i = 0; i < hex.size(); i += 2)
        v.push_back(byte_from_hex(hex[i], hex[i + 1]));

    return bin_str(v);
}


bin_str& bin_str::operator^=(const bin_str& rhs)
{
    for (std::size_t i = 0; i < data_.size(); ++i)
        data_[i] ^= rhs.data_[i];
    return *this;
}

std::string bin_str::ascii(bool ignore) const
{
    std::string s;
    for (std::size_t i = 0; i < data_.size(); ++i)
    {
        if (data_[i] < 0x20 || data_[i] > 0x7E)
        {
            if (ignore)
                continue;
            else
                throw std::runtime_error("invalid ascii");
        }
        s += data_[i];
    }
    return s;
}

void bin_str::add(unsigned long i)
{
    // we're too lazy to do this ourselves
    BIGNUM* tmp = BN_bin2bn(raw(), data_.size(), NULL);
    BN_add_word(tmp, i);
    if (BN_num_bytes(tmp) > data_.size())
        data_.resize(BN_num_bytes(tmp));
    BN_bn2bin(tmp, raw());
    BN_free(tmp);
}

}
