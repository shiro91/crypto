/*
Copyright (C) 2014 shiro <shiro@worldofcorecraft.com>
This program comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE.
*/

#ifndef CRYPTO__CTR_H
#define CRYPTO__CTR_H

#include "bin_str.h"
#include <stdexcept>

namespace crypto
{
namespace ctr
{

template <class block_cipher>
bin_str encrypt_iv(bin_str pt, bin_str key, block_cipher f_enc)
{
    // todo
    return bin_str();
}

/* first 16 bytes of ct is the IV */
template <class block_cipher>
bin_str decrypt(bin_str ct, bin_str key, block_cipher f_enc)
{
    if (ct.size() <= 16)
        throw std::runtime_error("Ciphertext too small");

    bin_str iv(ct.data().begin(), ct.data().begin() + 16);

    // figure out how many blocks there are in the ciphertext
    int blocks = (ct.size() - 16) / 16;
    if ((ct.size() - 16) % 16 != 0)
        ++blocks;

    bin_str pt;

    // decrypt block by block (could of course be parallelizable)
    for (int i = 0; i < blocks; ++i, iv.add(1))
    {
        bin_str curr_block(ct.data().begin() + (16 + i*16), ct.data().begin() + (32 + i*16));
        bin_str f_kiv = f_enc(iv, key);
        f_kiv ^= curr_block;
        pt.append(f_kiv);
    }

    // pop data that was not part of actual message
    int pop = 16 - ((ct.size() - 16) % 16);
    while (pop-- > 0)
        pt.data().pop_back();

    return pt;
}

}
}

#endif
