/*
Copyright (C) 2014 shiro <shiro@worldofcorecraft.com>
This program comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE.
*/

#ifndef CRYPTO__BIN_STR_H
#define CRYPTO__BIN_STR_H

#include <vector>
#include <string>
#include <algorithm>

namespace crypto
{

class bin_str
{
public:
    bin_str() { }
    bin_str(const std::vector<unsigned char>& d) : data_(d) { }
    template <class InputIterator>
    bin_str(InputIterator first, InputIterator last) : data_(first, last) { }

    const unsigned char* raw() const { return reinterpret_cast<const unsigned char*>(&data_[0]); }
    unsigned char* raw() { return reinterpret_cast<unsigned char*>(&data_[0]); }
    const std::vector<unsigned char>& data() const { return data_; }
    std::vector<unsigned char>& data() { return data_; }

    std::string hex() const;
    std::string bits() const;
    /* if ignore == true it simply skips non printable characters */
    std::string ascii(bool ignore = false) const;

    void resize(std::size_t i) { data_.resize(i); }
    std::size_t size() const { return data_.size(); }

    void append(const bin_str& s) { std::copy(s.data_.begin(), s.data_.end(), std::back_inserter(data_)); }

    void add(unsigned long i);

    // Operators
    bin_str& operator^=(const bin_str& rhs);

private:
    std::vector<unsigned char> data_;
};

bin_str bin_str_hex(const std::string& hex);
inline bin_str operator^(bin_str lhs, const bin_str& rhs)
{
    lhs ^= rhs;
    return lhs;
}

}

#endif
