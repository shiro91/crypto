/*
Copyright (C) 2014 shiro <shiro@worldofcorecraft.com>
This program comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE.
*/

#ifndef CRYPTO__CBC_H
#define CRYPTO__CBC_H

#include "bin_str.h"
#include <stdexcept>

namespace crypto
{
namespace cbc
{

template <class block_cipher>
bin_str encrypt_iv(bin_str pt, bin_str key, block_cipher f_enc)
{
    // todo
    return bin_str();
}

/* first 16 bytes of ct should be the initialization vector
   padding should be done as described in PKCS#7 */
template <class block_cipher>
bin_str decrypt(bin_str ct, bin_str key, block_cipher f_dec)
{
    if (ct.size() % 16 != 0)
        throw std::runtime_error("Invalid cipher-text size");
    if (key.size() != 16)
        throw std::runtime_error("Invalid key size");

    bin_str pt;

    // loop through one block at a time
    for (int i = 16; i < ct.size(); i += 16)
    {
        bin_str prev_block(ct.data().begin() + (i - 16), ct.data().begin() + i);
        bin_str curr_block(ct.data().begin() + i, ct.data().begin() + (i + 16));

        bin_str pt_block = f_dec(curr_block, key);
        pt_block ^= prev_block;

        pt.append(pt_block);
    }

    // remove padding
    int pad_len = pt.data()[pt.data().size() - 1];
    if (pad_len < 0 || pad_len > 16)
        throw std::runtime_error("Invalid padding length");
    while (pad_len-- > 0)
        pt.data().pop_back();

    return pt;
}

}
}

#endif
