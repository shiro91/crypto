/*
Copyright (C) 2014 shiro <shiro@worldofcorecraft.com>
This program comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE.
*/

#include <openssl/aes.h>
#include "aes.h"
#include <climits>
#include <stdexcept>

namespace crypto
{
namespace aes
{

bin_str encrypt(bin_str pt, bin_str key)
{
    AES_KEY aes_key;

    int ret = AES_set_encrypt_key(key.raw(), key.size() * CHAR_BIT, &aes_key);
    if (ret == -2)
        throw std::runtime_error("Key length not valid for AES");

    if (pt.size() != 16)
        throw std::runtime_error("Plaintext not valid length for AES");

    bin_str ct;
    ct.resize(16);
    AES_encrypt(pt.raw(), ct.raw(), &aes_key);

    return ct;
}

bin_str decrypt(bin_str ct, bin_str key)
{
    AES_KEY aes_key;

    int ret = AES_set_decrypt_key(key.raw(), key.size() * CHAR_BIT, &aes_key);
    if (ret == -2)
        throw std::runtime_error("Key length not valid for AES");

    if (ct.size() != 16)
        throw std::runtime_error("Ciphertext not valid length for AES");

    bin_str pt;
    pt.resize(16);
    AES_decrypt(ct.raw(), pt.raw(), &aes_key);

    return pt;
}

}
}
