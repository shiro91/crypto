/*
Copyright (C) 2014 shiro <shiro@worldofcorecraft.com>
This program comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE.
*/

#ifndef CRYPTO__AES_H
#define CRYPTO__AES_H

#include "bin_str.h"

namespace crypto
{
namespace aes
{

bin_str encrypt(bin_str pt, bin_str key);
bin_str decrypt(bin_str ct, bin_str key);

}
}

#endif
